# Lexical Stability and Emotion in Two Diachronic Corpora

This project contains data and code to reproduce the analyses described in our contribution:

Kern, Bettina M. J.; Hofmann, Klaus; Baumann, Andreas; Wissik, Tanja (submitted). Komparative Zeitreihenanalyse von lexikalischen Aspekten in österreichischen Korpusdaten: Stabilität und Emotion.

See abstract below for more information!

## Data
Data files contain extracted word fields as described in the manuscript. Note that we do not provide emotion scores (valence/arousal). These were taken from: Köper, Maximilian & Sabine Schulte im Walde. 2016. Automatically generated affective norms of abstractness, arousal, imageability and valence for 350 000 German Lemmas. In Proceedings of the 10th International Conference on Language Resources and Evaluation. 2595–2598.

## Requirements
For all computations, R was used. Additional requirements:
- tidyr
- rgl
- visreg
- mgcv
- itsadug
- ggplot2

## Abstract (German)
Anhand von zwei unterschiedlich aufgebauten Textkorpora wird untersucht, inwiefern sich lexikalischer Gebrauch und der emotionale Gehalt im Diskurs über politische Parteien in österreichischen Zeitungsbeiträgen und im Diskurs der Parteien selbst in ihren Reden vor dem österreichischen Nationalrat über die Zeit verändern. Welche Charakteristika des Diskurses verändern sich, auf welche Faktoren sind diese Dynamiken zurückzuführen und kann dies kontrastiv anhand von unterschiedlich aufgebauten Textkorpora untersucht werden? Im vorliegenden Beitrag wird anhand zweier Teilstudien diesen Fragen nachgegangen. Die beiden Teilstudien wenden eine komparative Zeitreihenanalyse, um die Dynamiken lexikalischer Stabilität und emotionalen Gehalts im typischen Wortgebrauch dreier österreichischer Parteien herauszuarbeiten. Für komparative Untersuchungen werden üblicherweise Vergleichskorpora verwendet; in dieser Studie wird hingegen auf sehr unterschiedlich aufgebaute diachrone Korpora zurückgegriffen: Das Austrian Media Corpus (AMC) und das Korpus der Österreichischen Parlamentsprotokolle (ParlAT). Um die Textkorpora, welche sich in ihrer Textsorte und Größe deutlich unterscheiden, als Datenbasis für die komparative Analyse nutzen zu können, wird in diesem Beitrag auf die notwendige spezielle Datenaufbereitung eingegangen und gezeigt, dass wenig datenintensive (insbesondere unüberwachte) Methoden zur quantitativen Messung von diachronen Dynamiken lexikalischen Gebrauchs eingesetzt werden können. Auf diese Weise werden auch kleine Datenmengen nutzbar gemacht. Exemplarisch werden die Methoden auf den politischen Diskurs von und über österreichische Parlamentsparteien angewandt, wobei insbesondere durch Kombination zweier psychologischer Maße (Valenz und Erregung) Rückschlüsse auf den emotionalen Status des jeweiligen Diskurses gezogen werden können. 
